package com.cinema.model;

import com.cinema.entity.CinemaHall;
import java.io.Serializable;
import org.springframework.data.repository.CrudRepository;

public interface CinemaHallRepository extends CrudRepository<CinemaHall, Serializable>
{
}
