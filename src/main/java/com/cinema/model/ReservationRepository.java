package com.cinema.model;

import com.cinema.entity.CinemaClient;
import com.cinema.entity.CinemaPlace;
import com.cinema.entity.Reservation;
import java.io.Serializable;
import org.springframework.data.repository.CrudRepository;

public interface ReservationRepository extends CrudRepository<Reservation, Serializable>
{
    public Reservation findOneByCinemaClientAndCinemaPlace(CinemaClient cinemaClient, CinemaPlace cinemaPlace);
}
