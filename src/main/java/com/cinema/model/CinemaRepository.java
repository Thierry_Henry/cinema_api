package com.cinema.model;

import com.cinema.entity.Cinema;
import java.io.Serializable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface CinemaRepository extends CrudRepository<Cinema, Serializable>
{
    
}
