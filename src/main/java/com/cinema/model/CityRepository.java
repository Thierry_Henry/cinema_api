package com.cinema.model;

import com.cinema.entity.City;
import java.io.Serializable;
import org.springframework.data.repository.CrudRepository;

public interface CityRepository extends CrudRepository<City, Serializable>
{
    public City findOneByName(String name);
}
