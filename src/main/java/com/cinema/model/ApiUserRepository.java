package com.cinema.model;

import com.cinema.entity.ApiUser;
import java.io.Serializable;
import org.springframework.data.repository.CrudRepository;

public interface ApiUserRepository extends CrudRepository<ApiUser, Serializable>
{
    public ApiUser findOneByName(String token);
}
