package com.cinema.model;

import com.cinema.entity.CinemaPlace;
import java.io.Serializable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface CinemaPlaceRepository extends CrudRepository<CinemaPlace, Serializable>
{

}
