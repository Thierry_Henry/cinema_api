package com.cinema.model;

import com.cinema.entity.CinemaClient;
import java.io.Serializable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface CinemaClientRepository extends CrudRepository<CinemaClient, Serializable>
{
}
