package com.cinema.model;

import com.cinema.entity.CinemaRow;
import java.io.Serializable;
import org.springframework.data.repository.CrudRepository;

public interface CinemaRowRepository extends CrudRepository<CinemaRow, Serializable>
{
}
