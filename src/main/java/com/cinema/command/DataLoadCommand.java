package com.cinema.command;

import com.cinema.entity.ApiUser;
import com.cinema.entity.Cinema;
import com.cinema.entity.CinemaClient;
import com.cinema.entity.CinemaHall;
import com.cinema.entity.CinemaPlace;
import com.cinema.entity.CinemaRow;
import com.cinema.entity.City;
import com.cinema.model.ApiUserRepository;
import com.cinema.model.CinemaClientRepository;
import com.cinema.model.CinemaHallRepository;
import com.cinema.model.CinemaPlaceRepository;
import com.cinema.model.CinemaRepository;
import com.cinema.model.CinemaRowRepository;
import com.cinema.model.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class DataLoadCommand implements CommandLineRunner
{
    @Autowired
    private ApiUserRepository apiUserRepository;
    
    @Autowired
    private CityRepository cityRepository;
    
    @Autowired
    private CinemaRepository cinemaRepository;
    
    @Autowired
    private CinemaHallRepository cinemaHallRepository;
    
    @Autowired
    private CinemaRowRepository cinemaRowRepository;
    
    @Autowired
    private CinemaPlaceRepository cinemaPlaceRepository;
    
    @Autowired
    private CinemaClientRepository cinemaClientRepository;
    
    @Override
    public void run(String... strings) throws Exception
    {
        loadUsers();
        loadCities();
        loadCinemas();
        loadHalls();
        loadRows();
        loadPlaces();
        loadClients();
    }
    
    private void loadUsers()
    {
        ApiUser test = new ApiUser();

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

        String hash = encoder.encode("s84jdf83s");

        test.setId(1);
        test.setName("apiuser");
        test.setPassword(hash);

        apiUserRepository.save(test);
    }
    
    private void loadCities()
    {
        City rzeszow = new City();
        rzeszow.setId(1);
        rzeszow.setName("rzeszow");
    
        City krakow = new City();
        krakow.setId(2);
        krakow.setName("krakow");

        cityRepository.save(rzeszow);
        cityRepository.save(krakow);
    }

    private void loadCinemas()
    {
        City rzeszow = cityRepository.findOneByName("rzeszow");
        Cinema testCinema = new Cinema();
        
        testCinema.setId(1);
        testCinema.setAddress("test address");
        testCinema.setCity(rzeszow);

        cinemaRepository.save(testCinema);
    }
    
    private void loadHalls()
    {
        Cinema testCinema = cinemaRepository.findOne(1);

        CinemaHall cinemaHall = new CinemaHall();
        cinemaHall.setId(1);
        cinemaHall.setCinema(testCinema);
        cinemaHall.setHallNumber(1);

        cinemaHallRepository.save(cinemaHall);
    }
    
    private void loadRows()
    {
        CinemaHall testHall = cinemaHallRepository.findOne(1);

        CinemaRow cinemaRow = new CinemaRow();
        cinemaRow.setId(1);
        cinemaRow.setRowNumber(1);
        cinemaRow.setCinemaHall(testHall);

        cinemaRowRepository.save(cinemaRow);
    }
    
    private void loadPlaces()
    {
        CinemaRow testRow = cinemaRowRepository.findOne(1);
        CinemaHall testHall = cinemaHallRepository.findOne(1);

        for(int i = 1; i < 11; i++) {
            
            CinemaPlace cinemaPlace = new CinemaPlace();
            cinemaPlace.setId(i);
            cinemaPlace.setCinemaRow(testRow);
            cinemaPlace.setCinemaHall(testHall);
            cinemaPlace.setPlaceNumber(i);

            cinemaPlaceRepository.save(cinemaPlace);   
        }
    }
    
    private void loadClients()
    {
        CinemaClient cinemaClient = new CinemaClient();
        cinemaClient.setId(1);
        cinemaClient.setName("test name");
        cinemaClient.setSurname("test surname");
        cinemaClient.setPhone("999888777");

        cinemaClientRepository.save(cinemaClient);
    }
}
