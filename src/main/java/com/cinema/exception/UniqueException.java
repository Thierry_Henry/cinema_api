package com.cinema.exception;

public class UniqueException extends InvalidFieldException
{   
    public UniqueException(String fieldName, String message)
    {
        super(fieldName, message);
    }
}
