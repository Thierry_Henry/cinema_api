package com.cinema.exception;

public class InvalidFieldException extends Exception
{
    protected String fieldName;
    
    public InvalidFieldException(String fieldName, String message)
    {
        super(message);
        this.fieldName = fieldName;
    }

    public String getFieldName()
    {
        return fieldName;
    }
}
