package com.cinema.service;

import com.cinema.entity.City;
import com.cinema.exception.UniqueException;
import com.cinema.model.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CityService
{
    @Autowired
    private CityRepository cityRepository;

    public void saveCity(City city) throws UniqueException
    {
        City existCity = cityRepository.findOneByName(city.getName());
        
        if (existCity != null) {
            
            throw new UniqueException("name", "City with name "+city.getName()+" already exists");
        }
        
        cityRepository.save(city);
    }
}
