package com.cinema.service;

import com.cinema.entity.CinemaClient;
import com.cinema.entity.CinemaPlace;
import com.cinema.entity.Reservation;
import com.cinema.exception.InvalidFieldException;
import com.cinema.exception.UniqueException;
import com.cinema.model.CinemaClientRepository;
import com.cinema.model.CinemaPlaceRepository;
import com.cinema.model.ReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ReservationService
{
    @Autowired
    ReservationRepository reservationRepository;
    
    @Autowired
    CinemaClientRepository cinemaClientRepository;
    
    @Autowired
    CinemaPlaceRepository cinemaPlaceRepository;

    public Reservation createReservation(int placeId, int clientId) throws InvalidFieldException
    {
        CinemaPlace existPlace = cinemaPlaceRepository.findOne(placeId);
        CinemaClient existClient = cinemaClientRepository.findOne(clientId);
        
        if (null == existClient) {
            
            throw new InvalidFieldException("Client id", "Client id is not valid");
        }
        
        if (null == existPlace) {

            throw new InvalidFieldException("Place id", "Place id is not valid");
        }
        
        Reservation existReservation = reservationRepository.findOneByCinemaClientAndCinemaPlace(existClient, existPlace);
        
        if (null != existReservation) {
            
            throw new UniqueException("reservation", "Reservation already exists");
        }
        
        Reservation reservation = new Reservation();
        reservation.setCinemaClient(existClient);
        reservation.setCinemaPlace(existPlace);
        
        reservationRepository.save(reservation);
        
        return reservation;
    }
}
