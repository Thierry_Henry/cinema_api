package com.cinema.controller;

import com.cinema.entity.CinemaPlace;
import com.cinema.model.CinemaHallRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.cinema.model.CinemaPlaceRepository;

@RestController
@RequestMapping("/api")
public class CinemaPlaceController
{
    @Autowired
    private CinemaPlaceRepository cinemaPlaceRepository;
    
    @RequestMapping("/getHallPlaces")
    public Iterable<CinemaPlace> getAllPlaces()
    {
        Iterable<CinemaPlace> allPlaces = cinemaPlaceRepository.findAll();
        
        return allPlaces;
    }
}
