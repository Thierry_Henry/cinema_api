package com.cinema.controller;

import com.cinema.entity.City;
import com.cinema.exception.UniqueException;
import com.cinema.model.CityRepository;
import com.cinema.service.CityService;
import com.cinema.validate.ValidateError;
import com.cinema.validate.ValidateErrorSerializer;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class CityController
{
    @Autowired
    private CityService cityService;
    
    @Autowired
    private CityRepository cityRepository;
    
    @RequestMapping(value="/addCity", method=RequestMethod.POST)
    @ResponseBody
    public ResponseEntity addCity(@Valid @RequestBody() City city , BindingResult result)
    {
        try {
         
            cityService.saveCity(city);
        } catch(UniqueException e) {
            
            return new ResponseEntity<>(new ValidateError(e.getFieldName(), e.getMessage()), HttpStatus.BAD_REQUEST);
        }
        
        if (result.hasErrors()) {
            
            return new ResponseEntity<>(ValidateErrorSerializer.getSerializedErrors(result), HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(city.getId(), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/editCity/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity editCity(@PathVariable int id, @Valid @RequestBody() City city, BindingResult result)
    {
        City existCity = cityRepository.findOne(id);
        
        if (null == existCity) {
            
            return new ResponseEntity<>("Resource: "+id+" not found", HttpStatus.NOT_FOUND);
        }

        existCity.setName(city.getName());

        try {

            cityService.saveCity(existCity);
        } catch (UniqueException e) {

            return new ResponseEntity<>(new ValidateError(e.getFieldName(), e.getMessage()), HttpStatus.BAD_REQUEST);
        }

        if (result.hasErrors()) {

            return new ResponseEntity<>(ValidateErrorSerializer.getSerializedErrors(result), HttpStatus.BAD_REQUEST);
        }
        
        return new ResponseEntity<>(existCity.getId(), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/getAllCities", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity getAll()
    {
        Iterable<City> allCities = cityRepository.findAll();

        return new ResponseEntity<>(allCities, HttpStatus.OK);
    }
}
