package com.cinema.controller;

import com.cinema.entity.Reservation;
import com.cinema.exception.InvalidFieldException;
import com.cinema.service.ReservationService;
import com.cinema.validate.ValidateError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ReservationController
{
    @Autowired
    ReservationService reservationService;
    
    @RequestMapping(value="/createReservation/{clientId}/{placeId}", method=RequestMethod.POST)
    @ResponseBody
    public ResponseEntity createReservation(@PathVariable int clientId, @PathVariable int placeId)
    {
        try {
            
            Reservation reservation = reservationService.createReservation(placeId, clientId);
            
            return new ResponseEntity<>(reservation.getId(), HttpStatus.OK);
        } catch (InvalidFieldException e) {
            
            return new ResponseEntity<>(new ValidateError(e.getFieldName(), e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }
}
