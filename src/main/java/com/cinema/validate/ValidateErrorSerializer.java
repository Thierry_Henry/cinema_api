package com.cinema.validate;

import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

public class ValidateErrorSerializer
{   
    public static List getSerializedErrors(BindingResult result)
    {
        List<ValidateError> errorsList = new ArrayList<>();
        
        for (ObjectError validateError : result.getAllErrors()) {
            
            ValidateError error = new ValidateError(validateError.getObjectName(), validateError.getDefaultMessage());
            
            errorsList.add(error);
        }
        
        return errorsList;
    }
}
