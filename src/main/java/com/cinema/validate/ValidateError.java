package com.cinema.validate;

import java.io.Serializable;

public class ValidateError implements Serializable
{
    private String objectName;
    private String errorMessage;
    
    public ValidateError(String objectName, String errorMessage)
    {
        this.objectName = objectName;
        this.errorMessage = errorMessage;
    }

    public String getObjectName()
    {
        return objectName;
    }

    public String getErrorMessage()
    {
        return errorMessage;
    }
}
