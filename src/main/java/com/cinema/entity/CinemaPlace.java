package com.cinema.entity;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "cinema_place")
public class CinemaPlace
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    
    @NotNull
    private int placeNumber;
    
    @ManyToOne
    @NotNull
    @JoinColumn(name = "cinema_hall_id")
    private CinemaHall cinemaHall;
    
    @ManyToOne
    @NotNull
    @JoinColumn(name = "cinema_row_id")
    private CinemaRow cinemaRow;
    
    @OneToOne(mappedBy = "cinemaPlace", cascade = CascadeType.ALL)
    private Reservation reservation;

    public void setId(int id)
    {
        this.id = id;
    }
    
    public int getId()
    {
        return id;
    }

    public long getPlaceNumber()
    {
        return placeNumber;
    }

    public void setPlaceNumber(int placeNumber)
    {
        this.placeNumber = placeNumber;
    }

    public CinemaHall getCinemaHall()
    {
        return cinemaHall;
    }

    public void setCinemaHall(CinemaHall cinemaHall)
    {
        this.cinemaHall = cinemaHall;
    }

    public CinemaRow getCinemaRow()
    {
        return cinemaRow;
    }

    public void setCinemaRow(CinemaRow cinemaRow)
    {
        this.cinemaRow = cinemaRow;
    }

    public Reservation getReservation()
    {
        return reservation;
    }

    public void setReservation(Reservation reservation)
    {
        this.reservation = reservation;
    }
}
