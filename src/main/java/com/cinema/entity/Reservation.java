package com.cinema.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "reservation")
public class Reservation
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    
    @NotNull
    @OneToOne(cascade = CascadeType.ALL)
    private CinemaPlace cinemaPlace;
    
    @ManyToOne
    @JoinColumn(name = "cinema_client_id", nullable = true)
    private CinemaClient cinemaClient;

    public void setId(int id)
    {
        this.id = id;
    }
    
    public long getId()
    {
        return id;
    }

    public CinemaClient getCinemaClient()
    {
        return cinemaClient;
    }

    public void setCinemaClient(CinemaClient cinemaClient)
    {
        this.cinemaClient = cinemaClient;
    }

    public CinemaPlace getCinemaPlace()
    {
        return cinemaPlace;
    }

    public void setCinemaPlace(CinemaPlace cinemaPlace)
    {
        this.cinemaPlace = cinemaPlace;
    }
}
