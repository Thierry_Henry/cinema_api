package com.cinema.entity;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "cinema_hall")
public class CinemaHall
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    
    @NotNull
    @Column(name="hall_number")
    private int hallNumber;
    
    @ManyToOne
    @NotNull
    @JoinColumn(name = "cinema_id")
    private Cinema cinema;
    
    @OneToMany(mappedBy = "cinemaHall", cascade = CascadeType.ALL)
    private List<CinemaRow> cinemaRows;
    
    @OneToMany(mappedBy = "cinemaHall", cascade = CascadeType.ALL)
    private List<CinemaPlace> cinemaPlaces;

    public void setId(int id)
    {
        this.id = id;
    }
    
    public long getId()
    {
        return id;
    }

    public int getHallNumber()
    {
        return hallNumber;
    }

    public void setHallNumber(int hallNumber)
    {
        this.hallNumber = hallNumber;
    }

    public Cinema getCinema()
    {
        return cinema;
    }

    public void setCinema(Cinema cinema)
    {
        this.cinema = cinema;
    }

    public List<CinemaRow> getCinemaRows()
    {
        return cinemaRows;
    }

    public void setCinemaRows(List<CinemaRow> cinemaRows)
    {
        this.cinemaRows = cinemaRows;
    }

    public List<CinemaPlace> getCinemaPlaces()
    {
        return cinemaPlaces;
    }

    public void setCinemaPlaces(List<CinemaPlace> cinemaPlaces)
    {
        this.cinemaPlaces = cinemaPlaces;
    }
}
