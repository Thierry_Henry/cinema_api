package com.cinema.entity;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "cinema_row")
public class CinemaRow
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    
    @NotNull
    @Column(name="row_number")
    private int rowNumber;
    
    @ManyToOne
    @NotNull
    @JoinColumn(name = "cinema_hall_id")
    private CinemaHall cinemaHall;
    
    @OneToMany(mappedBy = "cinemaRow", cascade = CascadeType.ALL)
    private List<CinemaPlace> cinemaPlaces;

    public void setId(int id)
    {
        this.id = id;
    }
    
    public int getId()
    {
        return id;
    }

    public int getRowNumber()
    {
        return rowNumber;
    }

    public void setRowNumber(int rowNumber)
    {
        this.rowNumber = rowNumber;
    }

    public CinemaHall getCinemaHall()
    {
        return cinemaHall;
    }

    public void setCinemaHall(CinemaHall cinemaHall)
    {
        this.cinemaHall = cinemaHall;
    }

    /**
     * @return the cinemaPlaces
     */
    public List<CinemaPlace> getCinemaPlaces()
    {
        return cinemaPlaces;
    }

    /**
     * @param cinemaPlaces the cinemaPlaces to set
     */
    public void setCinemaPlaces(List<CinemaPlace> cinemaPlaces)
    {
        this.cinemaPlaces = cinemaPlaces;
    }
}
